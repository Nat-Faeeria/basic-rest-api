DROP TABLE Users;

DROP TABLE Classified;

CREATE TABLE Users (
  id SERIAL PRIMARY KEY NOT NULL,
  username VARCHAR(255),
  password VARCHAR(255),
  date_naiss CHAR(10)
);

CREATE TABLE Classified (
  id SERIAL PRIMARY KEY NOT NULL,
  info VARCHAR(255)
);

INSERT INTO Users(username, password, date_naiss) VALUES ('machinchose', 'abcdef', '10/10/2010'), ('trucmuche', '12345', '24/09/2021');
INSERT INTO Classified(info) VALUES ('secret'), ('trés secret'), ('ultra secret');
